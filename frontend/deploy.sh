#!/usr/bin/env bash

# create an .env file to save your environment variable with this below {


# IMAGE_NAME="your_image_name"
# LOCAL_PORT="your_image_port"
# REPOSITORY_NAME="your_repo_name"

# }
source .env

docker ps --format "table {{.ID}}\t{{.Ports}}"
docker build -t $IMAGE_NAME . 
docker images ls
# docker run -d $IMAGE_NAME_VERSION
docker run -d -p $LOCAL_PORT:3000 $IMAGE_NAME
docker ps
echo "docker-build successful"

# push image to dockerhub
docker tag $IMAGE_NAME $REPOSITORY_NAME
docker push $REPOSITORY_NAME
echo "pushed to registry successful"

