## Microservice Application Docker Deployment
This README provides instructions for deploying a microservice application using Docker. The deployment process is automated through a provided bash script. Before you proceed, make sure to configure your environment variables in an .env file as described below:

###
```
change directory into any of the micro-service (frontend, backend) then follow the next instruction. 

cd frontend
```

#### Environment Configuration
Create an .env file in the project root and set the following environment variables:

```
#### Replace with your desired values
.env file

IMAGE_NAME="your_image_name"
LOCAL_PORT="your_local_port"
REPOSITORY_NAME="your_repo_name"
Make sure to replace the placeholder values with your actual configuration.
```

##### Deployment Script
The deployment script, deploy.sh, automates the process of building and running your Docker container. To use it, follow these steps:

Ensure Docker is installed on your system.
Grant execute permission to the script:
```
chmod +x deploy.sh
```
```
Run the script:
./deploy.sh
```

##### This script performs the following actions:
```

* Builds a Docker image using the specified IMAGE_NAME.

* Lists Docker images.

* Runs the Docker container, mapping the specified LOCAL_PORT to the container's port 3000.
* Lists running containers.
* After successful execution, your microservice application should be up and running in the Docker container.


------Pushing the Image to DockerHub---------
The deployment script also includes commands to push your Docker image to DockerHub. To push the image, follow these steps:

Make sure you are logged in to DockerHub using docker login.

This will build the Docker image and tag it with your REPOSITORY_NAME.

Push the tagged image to DockerHub:

$ docker push $REPOSITORY_NAME
This will upload your Docker image to DockerHub under the specified REPOSITORY_NAME.

After successful execution, your Docker image will be available on DockerHub or other repository (ECR, GCR) for others to use.

-----Summary-----
This README provides a simple guide to deploying your microservice application using Docker. By configuring the environment variables in the .env file and running the provided deployment script, you can easily build, run, and push your Docker image to DockerHub.
